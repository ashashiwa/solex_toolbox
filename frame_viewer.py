from PyQt5.QtWidgets import *
from PyQt5 import uic
from PyQt5.QtGui import *
from PyQt5.QtCore import pyqtSignal, QObject, QTimer
import sys, time
import numpy as np
from qimage2ndarray import gray2qimage
class UI(QLabel):
    drawpix = pyqtSignal(np.ndarray, name='colonne')
    def __init__(self, fichier,width=2000):
        
        super(UI, self).__init__()
        self.drawpix.connect(self.addPicture) #connection signal/slot pour rester dans le thread de l'appli.
        #uic.loadUi("viewer.ui", self)
        
        self.myFile = VideoFile(fichier)
        self.myFile.setCurrentPosition(0) #SER file initialisation 
 
        self.X=0            #compteur général  
        self.decal =500     #taile des sous-images.
        
        height = self.myFile.getHeight()
        column = np.zeros(height) 
        column = column[:,np.newaxis]
        h,w = column.shape
        
        self.liste_qpixmap = [] #liste des QPixmpa à afficher       
        self.image_a_ajouter = QImage(column.data, h, w, 1, QImage.Format_Grayscale8) #initialisation avec une colonne de 0
        self.frame_partielle = column.copy() 
        
        #############            A CHANGER           ############
        ############ DÉPEND DE LA GÉOMÉTRIE DE LA RAIES #########
        self.position_pixel = 55  
        
        ####lancement des timers TEST
        self.timer=QTimer()
        self.timer.timeout.connect(self.calcul_colonne_et_affiche)
        ##################################"
        
        ############## calcul des fps ###############
        self.debut = time.time()
        self.fps = 0
        
        self.resize(width, height)
        self.show()
    
    def paintEvent(self, event):
        debut= time.time()

        self.painter = QPainter()
        self.painter.begin(self)
        cptr=0
        for pixmap in self.liste_qpixmap : 
            self.painter.drawPixmap(cptr, 0, pixmap) #cette liste contient des morceaux de QPixmap de 500px.
            cptr+=self.decal
        self.painter.drawPixmap(cptr, 0, QPixmap.fromImage(gray2qimage(self.frame_partielle)))        
        self.painter.end()
 
    def addWidth(self):
        if (self.width()-self.X)<self.width()/10 : 
            self.setMinimumWidth(self.width()+self.decal)
        
    def addPicture(self,colonne =np.ones(800) ):
        self.X+=1
        if len(colonne.shape)<2:
            colonne = colonne[:,np.newaxis].astype('uint8') #reshape si besoin
        self.image_a_ajouter = gray2qimage(colonne.data) #self.imageà ajouter est la dernière colonne
        debut = time.time()
        if self.X%self.decal==0 : #découpe en plusieurs images
            grande_image = gray2qimage(self.frame_partielle)
            self.liste_qpixmap.append(QPixmap.fromImage(grande_image)) #enregistre l'image sous forme de QPixmap dans une liste.
            self.frame_partielle = colonne.copy() #réinitialise le tableau numpy


        self.frame_partielle = np.concatenate((self.frame_partielle, colonne), axis=1) #construit la grande image uniquement en numpy.
        
        temps = time.time()
        self.fps+=1
        if temps-self.debut > 1 :
            self.debut=time.time()
            print("fps = ", self.fps)
            self.fps=0

        #redimensionne si ça arrive vers la fin de la géométrie.
        self.addWidth()
        self.update()
    
    def addPix(self, colonne):
        self.drawpix.emit(colonne)
        
    def calcul_colonne_et_affiche(self):
        decal = 18
        frame,no = self.myFile.read() 
        if no<self.myFile.getLength():
            if self.X%1==0 : 
                """appelé par un timer, calcul la colonne de pixel et lance son affichage"""
                ray_is_vertical = True
                if self.myFile.getWidth() > self.myFile.getHeight() : #ray are horizontal
                    ray_is_vertical = False
                if not ray_is_vertical  : 
                    frame = np.transpose(frame)
                #np.save("frame_.npy", frame)
                frame_découpée_autour_raie = frame[:,self.position_pixel-decal:self.position_pixel+decal ]
                liste_mini = list_minima(frame_découpée_autour_raie)
                self.addPix((build_a_vertical_frame_from_list(frame, liste_mini+self.position_pixel-decal,0)/65635*500).astype('uint8'))
                self.X+=1
                return
        else : 
            self.timer.stop()
        
    def go(self) : 
        self.timer.start(1)
    def stop(self) : 
        self.timer.stop()
        
app = QApplication(sys.argv)
from toolbox_solex import *  #doit rester ici sou pein d'une erreur de thread.
fichier =  'Sun_104633.ser' ##A CHANGER. 


window = UI(fichier = fichier )    
window.go()


app.exec_()
