try : 
    from serfilesreader import Serfile
except: 
    from serfilesreader.serfilesreader import Serfile

import numpy as np
import numpy.ma as ma
from matplotlib import pyplot as plt 
#import PySimpleGUI as sg
import sys
import math
import copy,os

#from astropy import modeling
from scipy import optimize

from skimage import data, color, img_as_ubyte
from skimage.feature import canny
from skimage.transform import hough_ellipse
from skimage.draw import ellipse_perimeter
from scipy.interpolate import interp1d

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, FigureCanvasAgg
from matplotlib.figure import Figure
import matplotlib 
matplotlib.use('TkAgg')

from scipy.interpolate import interp1d, splrep, splev
import cv2

#########tests purpose#########
import functools,time
def time_it(func):
    """Timestamp decorator for dedicated functions"""
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start = time.time()                 
        result = func(*args, **kwargs)
        elapsed = time.time() - start
        mlsec = repr(elapsed).split('.')[1][:3]
        readable = time.strftime("%H:%M:%S.{}".format(mlsec), time.gmtime(elapsed))
        print('Function "{}": {} sec'.format(func.__name__, readable))
        return result
    return wrapper

class VideoFile():
    """abstraction class to handle different video types"""
    def __init__(self, filename):
        if filename.split('.')[-1].upper()=="SER":
            self.media_type = 'ser'
            self.object_video = Serfile(filename) 
            self.height, self.width = self.object_video.getHeight(), self.object_video.getWidth()
            self.length = int(self.object_video.getLength())
        #elif filename.split('.')[-1].upper()=="AVI":
        else:  #All video types handled by openCV
            self.media_type = 'avi' 
            self.object_video = cv2.VideoCapture(filename)
            self.height, self.width = int(self.object_video.get(4)), int(self.object_video.get(3))
            self.length = int(self.object_video.get(7))
        
        #get length
    def getLength(self):
        return self.length
    
    #get height
    def getHeight(self):
        return self.height
    
    #get width
    def getWidth(self): 
        return self.width
    
    #set position
    def setCurrentPosition(self,n):
        if self.media_type == 'ser':
            return self.object_video.setCurrentPosition(n)
        if self.media_type=='avi':
            return self.object_video.set(1,n) #index of the frame to be decoded/captured next.
        
    def read(self):
        """return frame read and number of frame if possible else None"""
        if self.media_type == 'ser':
            return self.object_video.read()
        elif self.media_type == 'avi':
            success,image = self.object_video.read()
            
            return np.array(cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)), success
        
@time_it
def sum_frames(serfile_, index_begin=0, index_end=None, normalized_=False, treshold=0) : 
    """return sum of all frames in ser files between index_begin (defautl 0) and index_end (default lenght of serfile)"""
    lenght = serfile_.getLength()
    serfile_.setCurrentPosition(0)
    if index_end == None:
        index_end=lenght
    sum_of_frames = np.zeros(serfile_.getWidth()*serfile_.getHeight())
    sum_of_frames = np.reshape(sum_of_frames,(serfile_.getHeight(),serfile_.getWidth()))
    for i in range( index_begin, index_end):
        frame = serfile_.read()[0]
        sum_of_frames+=np.where(frame<treshold, 0, frame)
    if normalized_ : 
        #return (65536*(sum_of_frames - np.min(sum_of_frames))/np.ptp(sum_of_frames)).astype('uint16')
        return normalized(sum_of_frames)
    return sum_of_frames

def save_to_png(frame, filename):
        if not 'png' in filename.split('.')[-1].lower() : 
            filename+='.png'
        #On 16 bits, it possible to have somme issue. So theses lines transform 16bits in 8 bits.
        index_maximum = np.amax(frame)
        datas = 256/index_maximum*frame.astype(int)
        return cv2.imwrite(filename, datas), filename

def normalized(frame):
    """normalized on 16 bits"""
    return (65535*(frame - np.min(frame))/np.ptp(frame)).astype('uint16')

def black_or_white_treshold(frame, treshold):
    """return a Black or White frame. Black under treshold, white either"""
    return np.where(frame>treshold, 65535, 0.1)

def gaussian(x, amplitude, mean, stddev):
    """gaussian curve equation"""
    return amplitude * np.exp(-((x - mean) / 4 / stddev)**2)

def all_minima_in_a_frame(one_frame):
    """take a frame and return a list of all value took at minimum+dx """
    idx_minima = np.zeros(one_frame.shape[0])
    for i in range(one_frame.shape[0]):
        pixel, valeur, is_ray = locate_middle_of_ray_astropy_min_max(one_frame[i])
        try :
            idx_minima[i] = int(pixel)
        except : 
            idx_minima[i] = 0 

    return idx_minima.astype('uint16')

def all_minima_in_a_frame_quad(one_frame):
    liste_px = all_minima_in_a_frame(one_frame)
    popt, i_min, i_max, perr = interpolate_quad(liste_px)
    ##on recalcule la posiiton des minima avec l'interpolation
    return np.array([quad(x_, *popt) for x_ in range(len(liste_px))])


def locate_middle_of_ray_astropy_min_max(one_line_frame):
    """With a line, return minimum, the index and if it's a frame"""
    length = one_line_frame.shape[0]

    ###is this a ray ? It's ray if  : 
    ###minimum is 3 times lower than mean at extremities
    x_min = np.argmin(one_line_frame)
    minimum = one_line_frame[x_min]
    mean_left = np.mean(one_line_frame[0:length//10])
    std_left= np.std(one_line_frame[0:length//10])
    std_right= np.std(one_line_frame[int(length*9/10):])
    mean_right = np.mean(one_line_frame[int(length*9/10):])
    is_ray = minimum < min(mean_left, mean_right)-3*max(std_left, std_right) #criteria for a ray. Minimum is lower than 3 std deviation
    x = np.arange(length) 
    y = one_line_frame
    if is_ray : 
        return x_min, minimum, True
    else : return 0,0, False

def locate_middle_of_ray_astropy_gaussian(one_line_frame):
    from scipy.signal import find_peaks
    """argument : numpy array of one line
    return : middle, value_min, fwhm"""
    
    #####careful !!! TODO not finished, not working #####
    
    ###keep 1/3 middle avoid some artifacts
    init_length = one_line_frame.shape[0]
    line = one_line_frame[init_length//5:4*init_length//5]
    length = line.shape[0]
    print('length',length)
    ###is this a ray ? It's ray if  : 
    ###there's 2 points at intensity/2
    x_min = np.argmin(line)
    print(x_min)
    minimum = line[x_min]
    print(minimum)
    mean_left = np.mean(line[0:length//10])
    std_left= np.std(line[0:length//10])
    std_right= np.std(line[int(length*9/10):])
    mean_right = np.mean(line[int(length*9/10):])
    is_ray = minimum < min(mean_left, mean_right)-2*max(std_left, std_right) #criteria for a ray. Minimum is lower tha 2 std deviation
    y = np.linspace(0,length, length)
    plt.plot(y, line)
    plt.show()
    
    if is_ray :
        ###compute about where is the ray guess 5 pixel left and right from minimum
        dx = 8
        mean_of_means = (mean_right+mean_left)/2
        ray = -(line[x_min-dx:x_min+dx]-mean_of_means) #baseline
        ray = ray-np.min(ray)
        length_ray = ray.shape[0]
        x = np.linspace(0,length_ray, length_ray)
        popt, _ = optimize.curve_fit(gaussian, x, ray)
        return round(popt[1]+x_min-10), mean_of_means-popt[0]-line[x_min-10], is_ray
    else : 
        return 0,0, is_ray
        
    return minimum, np.max(fitted_model(x)), mean_left, std_left, mean_right, std_left, is_ray
    
def roi_frame(frame, roi=None):
    """return a ROI (Region Of Interest) of a frame, i.e. a subrectangle. x_l, y_l, x_r, y_r"""
    if roi==None : 
        x_l, y_l, x_r, y_r = 0, 0,frame.shape[1], frame.shape[0]
    else: 
        x_l, y_l, x_r, y_r = roi
    return frame[y_l:y_r+1, x_l:x_r+1].copy(order='C')

def gaussian_v(frame, sigma) :
    """apply a gaussian vertical blur"""
    return cv2.GaussianBlur(frame,(0,0),1,sigma,cv2.BORDER_DEFAULT)

def gaussian_h(frame, sigma) :
    """apply a gaussian horizontal blur"""
    return cv2.GaussianBlur(frame,(0,0),sigma,1,cv2.BORDER_DEFAULT)
    
def frame_with_treshold(frame, treshold, width=10):
    """return binarized frame at treshold"""
    wantedSlotWidth = width 
    frameWithBlackSlot = black_or_white_treshold(frame, treshold)
    outputFrame = copy.deepcopy(frame)
    liste_pixel_tresh = []
    
    for num_line in range(len(frameWithBlackSlot)):
        firstPix = None
        lastPix = None
        
        for thisPix in range (len(outputFrame[num_line])):
            if (frame[num_line][thisPix] <treshold):
                break
        if (thisPix < len(frameWithBlackSlot[num_line])):
            liste_pixel_tresh.append(thisPix)
        
        #add width
        lastPix = thisPix+wantedSlotWidth
        
        for thisPix in range(len(frameWithBlackSlot[num_line])):
            outputFrame[num_line][thisPix] = 0

        if lastPix != None and firstPix != None:
            for thisPix in range(0,wantedSlotWidth):
                realPix = firstPix + ((lastPix - firstPix) / wantedSlotWidth) * thisPix
                outputFrame[num_line][thisPix + int((len(frameWithBlackSlot[num_line])-wantedSlotWidth)/2)] = frame[num_line][math.floor(realPix)] * (1-(realPix % 1)) + frame[num_line][math.ceil(realPix)] * (realPix % 1)
    return outputFrame
        
def show(frame, matpltlib=False): 
    if not matpltlib : 
        try : 
            cv2.imshow('image',frame)
            cv2.waitKey(0)
            cv2.destroyAllWindows()
        except : 
            plt.imshow(frame, interpolation='nearest')
            plt.gray()
            plt.show()
    else :
        y = frame
        x = np.arange(frame.shape[0])
        plt.plot(x,y)
        plt.show()
        
def list_minima(frame):
    """return a np.array containing X minimum, line by line.
    In : frame np.array
    """
    liste = []
    for i in range(frame.shape[0]):
        #print(np.where(frame[i] == np.min(frame[i]))[0][0])
        liste.append(int(np.where(frame[i] == np.min(frame[i]))[0][0]))
    return np.array(liste)
   
def slote_get_straight_with_minima(frame, liste):
    """return frame with some translations nearby middle"""
    liste = copy.deepcopy(liste)
    x_mini = min(liste)
    length = frame.shape[1]
    outputFrame = np.array(frame, copy=True)  
    for i in range(frame.shape[0]):
        
        position_mini = liste[i]
        #print(x_mini, position_mini)
        outputFrame[i][x_mini:x_mini+length-position_mini] = frame[i][position_mini:]
    return outputFrame
    
def gui_for_treshold(frame):
    """launch a GUI with a frame. Shown a binarized frame at treshold"""
    treshold = 25000
    layout = [
            [sg.Slider(range=(1,65535),
                    default_value=25000,
                    size=(20,15),
                    orientation='horizontal',
                    font=('Helvetica', 12),
                    key="slider")],
            [sg.Image(save_to_png(frame, 'tmp2.png')[1], key='image_depart'),
            sg.Image(save_to_png(frame_with_treshold(frame, treshold), 'tmp.png')[1], key='image_nb')],
    ]
    window = sg.Window('find the threshold',location=(800,400))
    window.Layout(layout)
    while True : 
        event, values = window.Read(timeout=0, timeout_key='timeout')
        if event == 'Exit' or event is None:
            break
        treshold = int(values['slider'])
        frameNB = black_or_white_treshold(frame, treshold)
        imgbytes=cv2.imencode('.png', frameNB)[1].tobytes() 
        window.FindElement('image_nb').Update(data=imgbytes)
        window.refresh()
    window.close()
    
def make_flat_line(entire_frame):
    """make a vertical line with mean of picture
    IN :    nummy array of entire frame
    OUT :   numpy array 1D -> a flat line"""
    method = 'mean' #'mean' or 'median'
    flat_line = np.zeros(entire_frame.shape[0])
        
    mean_corner = np.mean(entire_frame[:entire_frame.shape[0]*1//10, :entire_frame.shape[1]*1//10])
    
    ##clean picture : the aim is to take only pixels that are illuminated
    clean_frame = ma.masked_less(entire_frame, mean_corner*1.1) #mask all  pixel that are than backgrund + 10% dynamics
    #clean_frame = ma.masked_less(entire_frame-mean_corner, (np.max(entire_frame)-mean_corner)*0.5) #50%dynamics
    ####last works not well.
    
    if method=='median' : 
        liste_median_values_smoothed = get_normalized_flat_line(clean_frame,method) #this give a list of median smoothed values.
    else : 
        liste_mean_values_smoothed = get_normalized_flat_line(clean_frame) #this give a list of smoothed values. WORKS
    
    for i in range(entire_frame.shape[0]): ##for each line
        if method=='mean' : 
            line_value = np.mean(clean_frame[i]) 
            line_value_smoothed = liste_mean_values_smoothed[i] 
        
        else : #median
            line_value = np.median(clean_frame[i]) 
            line_value_smoothed = liste_median_values_smoothed[i] 
        
        flat_line[i] = line_value/line_value_smoothed
    return np.nan_to_num(flat_line, nan=1)

@time_it
def build_a_vertical_frame_from_list(frame, liste_px,dx=0,interpol=0):
    """with a frame and a list of index, extract a vertical line with this index
    dx : offset from minimum
    interpolation works if liste_px contains "float values" from a quadratic curve interpolation.
    interpol : if liste_px is a list of Float, compute an interpolation.
               0 : no interpolation
               1 : linear handmade interpolation 
               2 : use interpol1D from scipy 
               3 : use spline from scipy
    """
    minima = np.zeros(frame.shape[0])
    for i in range(frame.shape[0]):
        try : 
            if interpol == 0 : 
                minima[i] = frame[i][int(liste_px[i])+dx]
            if interpol==1 : #make an interpolation between 2 points
                px_float = liste_px[i]
                frame_value1 = int(frame[i][int(px_float)+dx])
                frame_value2 = int(frame[i][int(px_float)+dx+1])
                minima[i] = frame_value1+(frame_value2-frame_value1)*(px_float-int(px_float))
                
            if interpol==2 : #interpol1D
                px_float = liste_px[i]
                n = 2  #radius
                values = frame[i][int(px_float)+dx-n:int(px_float)+dx+n].copy()
                x = np.arange(int(px_float)+dx-n, int(px_float)+dx+n)
                interpol_  = interp1d(x, values)
                minima[i] = interpol_(px_float+dx)    
            if interpol==3 :  #spline
                px_float = liste_px[i]
                n = 2  #radius
                values = frame[i][int(px_float)+dx-n:int(px_float)+dx+n].copy()
                x = np.arange(int(px_float)+dx-n, int(px_float)+dx+n)
                interpol_  = splrep(x, values)
                minima[i] = splev(px_float+dx, interpol_)    
        except: ##when dx is too far
            pass
    return minima.astype('uint16')

def remove_transversalium(picture, debug=False):
    """take a picture and remove transversalium.
    IN : numpy array with a transversalium
    out : numpy array without tranversalium"""
    print("####making a Flat Line####")
    ###make an flat line to avoid tansversalium
    
    #TODO propager le décalage en pixel dx
    flat_line = make_flat_line(picture)
    
    flat_picture = np.repeat(flat_line, picture.shape[1]).reshape(flat_line.shape[0], picture.shape[1])
    show(flat_picture)
    return picture/flat_picture

@time_it
def combine_all_frames_from_file_auto(filename, debug=False, dx=0):
    """make all treatment and return picture Final and Flat picture"""

    myFile = VideoFile(filename)
    print("""####opening %s, it's a %s file.####"""%(filename, myFile.media_type))
        
    ray_is_vertical = True
    if myFile.getWidth() > myFile.getHeight() : #ray are horizontal
        ray_is_vertical = False
    
    print('####compute liste of minimum for all lines###')
    myFile.setCurrentPosition(myFile.getLength()//2)
    if ray_is_vertical : 
        frame = myFile.read()[0]
    else : #horizontal frame
        frame = myFile.read()[0]
        frame = np.transpose(frame)
    print("width : %s, height : %s, vertical ? %s, frame shape %s "%(myFile.getWidth(),myFile.getHeight(), ray_is_vertical, frame.shape ))

        
    liste_px = all_minima_in_a_frame(frame) #retrieve minimum of each line.
    print("liste of minima :", liste_px, "nb of minima :", len(liste_px))
    x = np.arange(0,len(liste_px),1)
    y = liste_px
    print("####use quadratic interpolation on minima")
    popt, i_min, i_max, perr = interpolate_quad(liste_px)
    #print("coefficients", popt, "begin of enlight slot pixel", i_min,"end of enlight slot pixel", frame.shape[1]//2+i_max,"max error",  np.max(perr))
    print("recompute with interpolation")
    liste_px = np.array([quad(x_, *popt) for x_ in range(len(liste_px))])
    
    print("####check if interpolate is fine#### if not, tweak lines in function nterpolate_quad()  """)
    
    #####deCOMMENT if you want to check
    plt.plot(x,y, x, liste_px)
    plt.title("check if interpolation is good. Mininum position with frame number")
    
    plt.show()
    
    
    SHG_picture = np.array([])   #initialisation of finale picture
    myFile.setCurrentPosition(0) #SER file initialisation 
    
    for i in range(myFile.getLength()):
        frame,no = myFile.read() 
        if not ray_is_vertical  : 
            frame = np.transpose(frame)
        if i==0 : 
            SHG_picture =  build_a_vertical_frame_from_list(frame, liste_px,dx)    
        else:  
            vertical_frame = build_a_vertical_frame_from_list(frame, liste_px,dx)
            SHG_picture = np.concatenate((SHG_picture, vertical_frame))
            
    #picture obtained is not in good shape -> transposition
    SHG_picture = np.transpose(SHG_picture.reshape(int(SHG_picture.shape[0]/len(liste_px)),  len(liste_px), ))
    
    return SHG_picture.astype('float64')

def colorize(image, color_='#c16725'):
    """return an colored picture
    IN : gray picture (numpy array) and color as in CSS
    OUT : color picture (numpy array) RGB colored""" 
    import skimage
    colored_image_still_grey = skimage.color.gray2rgb(image)
    ##color treatment
    color_ = color_[1:] ##remove '#'
    r = int(color_[:2], 16)  
    g = int(color_[2:4], 16)
    b = int(color_[4:6], 16)

    ####I don't understand why it's b,g, r instead of r, g, b... may be a shape issue.
    colored_image = [b/256,g/256,r/256]*colored_image_still_grey

    return colored_image.astype('uint8')

@time_it
def combine_all_frames_from_file_with_3px_width(filename_src, filename_dest, roi=None):
    #####don't use yet####
    file_src = Serfile(filename_src)
    file_dest = Serfile(filename_dest,NEW=True)
    file_dest_slant = Serfile(''.join(filename_dest.split('.')[:-1])+'_slant.ser',NEW=True)
    
    header = file_src.getHeader()
    file_dest.createNewHeader(header)
    file_dest_slant.createNewHeader(header)

    #image_norm_roi = roi_frame(sum_frames(file_src,800,3200,True,0), roi)
    
    
    #Make a gaussian verticla blurred to determine minma
    #image_norm_gauss_roi = gaussian_v(image_norm_roi,12)
    #show(image_norm_gauss_roi)
    
    #reset file.
    file_src.setCurrentPosition(0)
    
    #Make minima per lines
    liste = list_minima(image_norm_gauss_roi)
    
    #slot isolation
    slant_middle=53
    slant_half_width=2 
    
    #init file with Slot
    calcul=True
    
    if calcul : 
        for i in range(file_src.getLength()):
            frame = roi_frame(file_src.read()[0], roi)
            frame_straight = slote_get_straight_with_minima(frame,liste)
            #print(frame_straight, frame_straight.shape, slant_middle-slant_half_width, roi[1], slant_middle+slant_half_width, roi[3])
            frame_straight_with_only_slot = roi_frame(frame_straight, (slant_middle-slant_half_width, roi[1], slant_middle+slant_half_width, roi[3]))

            file_dest.addFrame(frame_straight)
            file_dest_slant.addFrame(frame_straight_with_only_slot)
            if i==0 : #fist frame, init sun_picture
                sun_picture = frame_straight_with_only_slot
            else : 
                sun_picture = np.concatenate((sun_picture, frame_straight_with_only_slot), axis=1)        
    return sun_picture

def img16b_to_8b(frame16b):
    frame8b = ((frame16b/(2**16-1))*256).astype('uint8')
    return frame8b

def pre_circularize(img, height=None):
    """try to circularize a SHG picture
    Assume film is sympetric and resize with the same width and height"""
    ###assume is a symetric film.   
    if height is None : 
        img = cv2.resize(img, dsize=(img.shape[0], img.shape[0]), interpolation=cv2.INTER_AREA)    
    else : 
        img = cv2.resize(img, dsize=(height, height), interpolation=cv2.INTER_AREA)    
    return img

def circularize(img, height=None):
    """compute center and diameter assuming the oval is X tranlated
    IN : picture (numpy array) to circularize
    
    OUT : picture (numpy array) circularized picture
    """
    print("####Circularization###")
    mean_corner = np.mean(img[:img.shape[0]*1//10, :img.shape[1]*1//10])

    clean_frame = img-np.min(img)
    
    ##assume corner top left first 10% are black.
    mean_corner = np.mean(clean_frame[:clean_frame.shape[0]*1//10, :clean_frame.shape[1]*1//10])

    clean_frame = np.where(img>(np.max(img)-mean_corner)*0.5, 1, 0) ##binarize at 50%% dynamics

    list_moment_x = np.sum(clean_frame, axis=0)
    list_moment_x_n = np.zeros(len(list_moment_x))
    moyenne_debut_x = np.mean(list_moment_x[:len(list_moment_x)//20])
    for i in range(len(list_moment_x)):
        list_moment_x_n[i] = list_moment_x[i]/list_moment_x[i] if list_moment_x[i]>moyenne_debut_x*2 else 0

    ##maximum of sum  give the middle
    list_moment_y = np.sum(clean_frame, axis=1)
    
    list_moment_y_n = np.zeros(len(list_moment_y))
    moyenne_debut_y = np.mean(list_moment_y[:len(list_moment_y)//20])
    for i in range(len(list_moment_y)):
        list_moment_y_n[i] = list_moment_y[i]/list_moment_y[i] if list_moment_y[i]>moyenne_debut_y*2 else 0

    print('center X', np.argmax(list_moment_x), 'center Y', np.argmax(list_moment_y))
    center_circle = (np.argmax(list_moment_y), np.argmax(list_moment_x))
    
    ##sum of normalisation give the diameter
    diameter_y = np.sum(list_moment_y_n)
    diameter_x = np.sum(list_moment_x_n)
    print('diameter : X and Y', diameter_x, diameter_y)
    
    
    width = img.shape[1]
    width_circle = diameter_x
    height_circle =  diameter_y

    ratio_circle = width_circle/height_circle
    
    if height is None : 
        height = img.shape[0]
    
    old_height = img.shape[0]
    ratio_h = old_height/height
    width_after = int(width/ratio_h/ratio_circle)
    
    new_img = cv2.resize(img, dsize=(width_after,height), interpolation=cv2.INTER_AREA)    
    
    return new_img
    
            
@time_it
def interpolate_quad(liste, deg=2):
    """compute a quadratic curve from a liste of pixels"""
    ###clean liste : there's some black part atleft and right. Theses are 0 Values.
    ###from middle to left, when a 0 is meet, i_min. Idem from middle to left.
    half_length = len(liste)//2
    i_min=half_length//2 #nothing is found
    for i in range(half_length):
        if liste[half_length-i]==0:
            i_min = i
            break

    ##idem for right : 
    i_max = half_length//2+1 #value if nothing is found
    for k in range(half_length):
        if liste[half_length+k]==0:
            i_max = k
            break
    ###optimize
    i=0 #counter of loop. Avoid infinite loop if computing is divergente.
    perr=(5,6)
    
    #####TWEAK HERE####
    while np.max(perr)>0.13 and i<3: #avoid infinite loop 0.13 seems to be a good treshold for video. May be camera sensitive. TODO Could be improved
        ##restraint i_min and i_max
        i_min = int(i_min*(1-i*0.05)) if i_min>0 else i_min  ##shift i_min about 5% on right
        i_max = int(i_max*(1-i*0.05)) if i_max>0 else i_max ##shift i_max about 5% on left
        
        new_length_liste = len(liste[half_length-i_min:half_length+i_max])
        #interpolate
        if deg==2 : #quadratic optimization
            popt, _ = optimize.curve_fit(quad, np.arange(half_length-i_min,half_length+i_max), liste[half_length-i_min:half_length+i_max], method='lm') #optimize in i_min, alf_length+i_max interval.
        elif deg==4 : 
            popt, _ = optimize.curve_fit(fourpower, np.arange(half_length-i_min,half_length+i_max), liste[half_length-i_min:half_length+i_max]) #optimize in i_min, alf_length+i_max interval.
        perr = np.sqrt(np.diag(_))
        i+=1
        ##-----------------------------------------
    
    return popt, i_min, i_max, perr

def quad(x, a, b, c):
    return a*x**2+b*x+c

def fourpower(x, a, b, c, d, e):
    return a*x**4+b*x**3+c*x**2+d*x+e

@time_it
def roi_frame_with_minima(frame, radius, liste_minima_px, interpol=0):
    for dx in range(-radius,radius) : 
        verticale_line = build_a_vertical_frame_from_list(frame,liste_minima_px,dx,interpol).reshape(h,1).astype('uint16')
        if dx==-radius : #first frame
            new_frame = verticale_line
        else : 
            new_frame = np.concatenate((new_frame, verticale_line.reshape(h,1)), axis=1)
    return new_frame

def get_normalized_flat_line(picture_cleaned, method='mean'): ##picture_cleaned is a masked array
    """with a picture_cleaned, return a smoothed one. Smooth ca be mean calculated or median calculated
    IN : entire frame numpy array
    OUT : entire numpy array smoothed"""
    print('OK', picture_cleaned.shape,len(picture_cleaned))
    baseline=[]
    dx = 15 #number of pixel on which horizontal mean is done.
    for i in range(dx, picture_cleaned.shape[1]-dx):
        try : 
            if method=='mean' : #compute mean
                baseline.append(np.mean([picture_cleaned[i-dx:i+dx]]))
            else : 
                baseline.append(np.median([picture_cleaned[i-dx:i+dx]]))
        except IndexError: 
            baseline.append(picture_cleaned[i])
    baseline = baseline[0:dx]+baseline+baseline[-dx:]        
    
    ####decomment to plot baseline####
    xnew = [x for x in range(len(baseline))]
    ynew = baseline
    #plt.plot(xnew, picture_cleaned.filled(0)[1], 'o', xnew, ynew, 'x')
    #plt.show()
    
    return baseline
    
if __name__ == '__main__':
    
    demo = False
    if demo : 
        ##############TOUT AUTO##############################
            
        #rep = "/home/jb/Téléchargements" #répertoire où sont les fichiers "ser".  A CHANGER
        rep = '.'
        fichier =  'Sun_104633.ser' ##A CHANGER. 
        #fichier =  '16_33_15.ser' ##A CHANGER. 
        #fichier = 'Ha0002_h.avi'

        
        image_centre_raie= combine_all_frames_from_file_auto(os.path.join(rep,fichier), dx=0) ###pas de decalage
        show(normalized(image_centre_raie))
        image_centre_raie_t = remove_transversalium(image_centre_raie)     #enleve le transversalium
                    
        show(circularize(normalized(image_centre_raie_t)))
        
        
        image_centre_raie= combine_all_frames_from_file_auto(os.path.join(rep,fichier), dx=-10) ###pas de decalage
        ##show(normalized(image_centre_raie))
        image_centre_raie_t = remove_transversalium(image_centre_raie)     #enleve le transversalium
        image_centre_raie_tn = normalized(image_centre_raie_t)              #normalise pour affichage
        show(circularize(normalized(image_centre_raie)))
        show(circularize(image_centre_raie_tn))
        
        
        #show(colorize(img16b_to_8b(circularize(image_centre_raie_tn))))
        #nom_png = "Sun_104633.png"
        #save_to_png(circularize(image_centre_raie_tn), nom_png)
    
    #####################DEMO#############################
    demo_totale = False
    demo_disque = False
    demo_straight_slit_ser_file = True
    if demo_totale : 
        rep = "/home/jb/Téléchargements" #répertoire où sont les fichiers "ser".  A CHANGER
        files = os.listdir(rep)
        #print(files)
        height = 800
        
        finale_image = np.zeros((height,1), dtype='uint8')
        clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
        for fichier in files : 
            if fichier.split('.')[-1]=='ser' :
                print("image de %s"%(fichier), os.path.join(rep,fichier))
                
                fichier =  'Sun_104633.ser' ##A CHANGER. 

                centre_ray= combine_all_frames_from_file_auto(os.path.join(rep,fichier), dx=0) ###no decal
                show(centre_ray)
                centre_ray_t = remove_transversalium(centre_ray)
                show(centre_ray_t)
                centre_ray_tn = normalized(centre_ray_t)
                
                SHG_picture2 = combine_all_frames_from_file_auto(os.path.join(rep,fichier), 0,1) #with FLAT
                SHG_picture2_t = remove_transversalium(SHG_picture2)
                SHG_picture2_tn = normalized(SHG_picture2_t)
                
                SHG_picture3 = combine_all_frames_from_file_auto(os.path.join(rep,fichier), 0,-1) #with FLAT
                SHG_picture3_t = remove_transversalium(SHG_picture3)
                SHG_picture3_tn = normalized(SHG_picture3_t)
                
                SHG_picture5 = combine_all_frames_from_file_auto(os.path.join(rep,fichier), 0,10) #with FLAT
                SHG_picture5_t = remove_transversalium(SHG_picture5)
                SHG_picture5_tn = normalized(SHG_picture5_t)
                
                doppler = remove_transversalium(SHG_picture3-SHG_picture2)

                doppler_tn = normalized(np.nan_to_num(doppler, nan=0))
                
                ######SAVING IN PNG###########
                nom_png = '_'.join(fichier.split('.')[:-1])+'.png'
                #save_to_png(circularize(SHG_picture2_n), nom_png)
                show(centre_ray_tn)
                finale_image = np.hstack((finale_image, circularize(centre_ray_tn, height)))
                finale_image = np.hstack((finale_image, circularize(SHG_picture2_tn, height)))
                finale_image = np.hstack((finale_image, circularize(SHG_picture3_tn, height)))
                finale_image = np.hstack((finale_image, circularize(doppler_tn, height)))
                finale_image = np.hstack((finale_image, circularize(SHG_picture5_tn, height)))
                break
        show(finale_image)
        #show(colorize(img16b_to_8b(finale_image)))
        
    if demo_disque :
        SHG_picture = combine_all_frames_from_file_auto("/home/jb/Téléchargements/11_40_09.ser", debug=True )
        #show(SHG_picture)
        show(pre_circularize(SHG_picture))
        clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
        show(clahe.apply(img16b_to_8b(pre_circularize(SHG_picture))))
        
    if demo_straight_slit_ser_file : 
        fichier =  'Sun_104633.ser'
        ####open as Serfile : 
        serfile = Serfile(fichier)
        
#############récupérer les valeurs des minimum de la raie
        ####se placer au milieu/Place at middle##### On présume que le film est équilibré et qu'au milieu on a une belle fente TOTALE
        serfile.setCurrentPosition(serfile.getLength()//2)
        frame,ret = serfile.read()
        liste_minima_px = all_minima_in_a_frame_quad(frame)
##############################################
########initialiser les valeurs
        h = serfile.getHeight()
        w = 40                  ##on ressert sur la fente
        
#création nouveau fichier ser
        new_serfile = Serfile('demo_ligne_droite__.ser',NEW=1)  #le NEW=1 permet de créer un nouveau fichier.
        new_serfile.createNewHeader()                         #intialise le header a des valeurs par defaut.
        new_serfile.setImageWidth(w)                          #gére la taille
        new_serfile.setImageHeight(h)
        new_serfile.setPixelDepthPerPlane(16)                 #Profondeur des données, en bits.
        
        #on se place à la 1000e ligne
        position=serfile.setCurrentPosition(1200)
        while position <1250 : #on traite 250 frames
            frame, position = serfile.read()

            ##on extrait la valeur des pixels sur un écart de 20px avant et 19pixels après
            new_frame = roi_frame_with_minima(frame, 20, liste_minima_px, interpol=0)
            
            
            new_serfile.addFrame(new_frame)
            print(position)
        position=serfile.setCurrentPosition(1200)
        
        liste_frame = []
        while position <1250 : #on traite 250 frames
            frame, position = serfile.read()

            ##on extrait la valeur des pixels sur un écart de 20px avant et 19pixels après
            new_frame = roi_frame_with_minima(frame, 20, liste_minima_px, interpol=1)
            
            
            new_serfile.addFrame(new_frame)
            liste_frame.append(new_frame)
            print(position)

        position=serfile.setCurrentPosition(1200)
        while position <1250 : #on traite 250 frames
            frame, position = serfile.read()

            ##on extrait la valeur des pixels sur un écart de 20px avant et 19pixels après
            new_frame = roi_frame_with_minima(frame, 20, liste_minima_px, interpol=3)
            
            
            new_serfile.addFrame(new_frame)
            liste_frame.append(new_frame)
            print(position)
    
        for framenb in range(50):
            
            new_frame = liste_frame[framenb]- liste_frame[framenb+50]
            
            
            new_serfile.addFrame(new_frame)
            print(framenb)

        
        
        
        

    
