from PIL import Image, ImageTk, ImageSequence
from serfilesreader.serfilesreader import Serfile
import PySimpleGUI as sg
import cv2
import time
import numpy as np


sg.LOOK_AND_FEEL_TABLE['darkTheme'] = {'BACKGROUND': '#18191c',
                                        'TEXT': '#dcddde',
                                        'INPUT': '#2f3136',
                                        'TEXT_INPUT': '#dcddde',
                                        'SCROLL': '#c7e78b',
                                        'BUTTON': ('#dcddde', '#36393f'),
                                        'PROGRESS': ('#01826B', '#D0D0D0'),
                                        'BORDER': 1, 'SLIDER_DEPTH': 0, 'PROGRESS_DEPTH': 0,
                                        }

sg.theme('darkTheme')   # Add a touch of color

image_play = './images/play.png'
image_pause = './images/pause.png'
image_slower = './images/pause.png'
image_faster = './images/faster.png'
image_slower = './images/slower.png'
image_bbracket = './images/bBracket.png'
image_ebracket = './images/eBracket.png'

playerControls = [[sg.ReadFormButton('',button_color=sg.TRANSPARENT_BUTTON, image_filename=image_bbracket, key='_BBRACKET_', image_size=(25, 25), image_subsample=2, border_width=0), sg.Text(' '),
                     sg.ReadFormButton('', button_color=sg.TRANSPARENT_BUTTON, image_filename=image_slower, image_size=(30, 30), key='_SLOWER_', image_subsample=2, border_width=0), sg.Text(' '),
                     sg.ReadFormButton('', button_color=sg.TRANSPARENT_BUTTON, image_filename=image_play, key='_PLAYPAUSE_', image_size=(50, 50), image_subsample=2, border_width=0), sg.Text(' '),
                     sg.ReadFormButton('', button_color=sg.TRANSPARENT_BUTTON, image_filename=image_faster, key='_FASTER_', image_size=(30, 30), image_subsample=2, border_width=0), sg.Text(' '),
                     sg.ReadFormButton('', button_color=sg.TRANSPARENT_BUTTON, image_filename=image_ebracket, key='_EBRACKET_', image_size=(25, 25), image_subsample=2, border_width=0), sg.Text(' ')
                  ]
]


playerContainer = [[sg.Text("Frame n°", font=('Any 12',9)), sg.Text("1", key="_NUMFRAME_", font=('Any 12',15, "bold"), size=(40,1))],
                    [sg.Canvas(size=(500, 62), background_color='#18191c', key='canvas')],
                    [sg.Column(playerControls,justification='center')]
                  ]
frameViewer = [[sg.Image(filename='', key='frameViewer', visible=False)]]

savingAsContainer = [[sg.FileSaveAs(key='SavingLocation', file_types=(("SER Files", "*.ser"),))]]

body = [  [sg.Input(key='_FILEBROWSE_', enable_events=True, visible=False),sg.Column(savingAsContainer, key='_SAVINGASCONTAINER_', visible=False)],  
          [sg.FileBrowse(file_types=(("SER Files", "*.ser"),), key='_FILEBROWSER_', target="_FILEBROWSE_") ,sg.Text("", key="errorBrowse", text_color="red", size=(40,1), visible=False)],
          [sg.Column(frameViewer), sg.Column(playerContainer, key='_CANVASCONTAINER_', size=(500, 300), visible=False)],
          [sg.ProgressBar(1000, orientation='h', size=(20, 20), key='progressbar', visible=False)]

]


layout = [ [sg.Frame('1) Please choose the SER File you want to cut', body, key='_ACTIVITYZONE_', font=('Any 12',9, "bold"))], 
        [sg.Button('Next >>', disabled=True, key='_NEXT_')]]



# Create the Window
window = sg.Window('SER Cutter', layout, size=(700,110), element_justification='c', finalize=True)


canvas = window['canvas']

timeline = canvas.TKCanvas.create_rectangle(50, 50, 450, 56, fill="#8e9297")

bracketBeginning = []
bracketBeginning.append(canvas.TKCanvas.create_line(50, 45, 50, 61, fill='white'))
bracketBeginning.append(canvas.TKCanvas.create_line(50, 45, 55, 45, fill='white'))
bracketBeginning.append(canvas.TKCanvas.create_line(50, 61, 55, 61, fill='white'))
bracketBeginning.append(50)

bracketEnd = []
bracketEnd.append(canvas.TKCanvas.create_line(450, 45, 450, 61, fill='white'))
bracketEnd.append(canvas.TKCanvas.create_line(450, 45, 445, 45, fill='white'))
bracketEnd.append(canvas.TKCanvas.create_line(450, 61, 445, 61, fill='white'))
bracketEnd.append(450)


playing = False
actualFrame = 0


timeMarker = canvas.TKCanvas.create_oval(50,50,56,56,fill='#01abea')

def moveCursor(event):
    global actualFrame, playing
    #playing=False
    moveTimeMarker(timeMarker, actualFrame, int(min(max(0,(event.x - 50) / 400 * serfile._length), serfile._length-1)),serfile._length)
    actualFrame = int(min(max(0,(event.x - 50) / 400 * serfile._length), serfile._length-1))
    frame, ret = read_frame(int(actualFrame))
    frame = cv2.resize(frame, (int(windowSize[1] * ratio * 0.8), int(windowSize[1] * 0.8))) 
    imgbytes=cv2.imencode('.png', frame)[1].tobytes() #ditto
    window['frameViewer'].update(data=imgbytes)
    window['_NUMFRAME_'].update("%i"%(ret))
    
    
                                         
canvas.Widget.bind('<Button-1>', moveCursor)
canvas.Widget.bind('<ButtonRelease-1>', moveCursor)
canvas.Widget.bind('<B1-Motion>', moveCursor)

def moveBracketTo(bracket,numFrame, nbFrames):
    lastPlace = bracket[3]
    bracket[3] = int(numFrame*(400/nbFrames)) + 50
    for numPartBracket in range (3):
        canvas.TKCanvas.move(bracket[numPartBracket], bracket[3]-lastPlace, 0)

def moveTimeMarker(timeMarker, lastPosition, newPosition, nbFrames):
    canvas.TKCanvas.move(timeMarker, (newPosition - lastPosition) * 400 / nbFrames, 0)


lastTime = time.time()
timeDelay = 0.2
framesToAdd = 1
frames = []
numFramePrinted = 0

firstFrame = 0
lastFrame = 0

actualFrame = 0

inputFile = ""
outputFile = ""

step = 0

def read_frame(n):
    if type(frames[n])!=np.ndarray: ##there's no frame, just a 0
        serfile.setCurrentPosition(n)
        frame_,ret = serfile.read()
        frames[n] = frame_
    else : ret = n
    return frames[n],ret

# Event Loop to process "events" and get the "values" of the inputs
while True:

    event, values = window.read(timeout=0)
    if event == sg.WIN_CLOSED or event == 'Cancel': # if user closes window or clicks cancel
        break
    elif event == "_FILEBROWSE_":
        
        #must read the readers and check file integrity, and write nbFrames and fileError below
            #location of the input SER file : values['Browse']
            #location where we have to save the output file : values['SavingLocation']
        #try:
        serfile = Serfile(values['_FILEBROWSE_'])
        
        window['_NEXT_'].update(disabled=False)
        window['errorBrowse'].update(value="", visible=False)
        firstFrame = 0
        lastFrame = serfile.getLength() - 1
        frames = [0]*serfile.getLength()
        #for numFrame in range(serfile.getLength()):
            #frames.append(serfile.read())
        #except:
            #window['errorBrowse'].update(value="Error: invalid file", visible=True)

    elif event == "_NEXT_":
        step +=1
        if step == 1:
            #window.Maximize()
            inputFile = values['_FILEBROWSE_']
            #windowSize = window.Size
            window.Size = (int(window.get_screen_size()[0]*0.99), int(window.get_screen_size()[1]*0.9))
            window.Move(0,0)
            windowSize = window.Size
            frame, ret = read_frame(0)
            ratio = len(frame[0]) / len(frame)
            window['_FILEBROWSER_'].update(visible=False)
            window['_ACTIVITYZONE_'].update("2) Select the cropping interval")
            frame = cv2.resize(frame, (int(windowSize[1] * ratio * 0.8), int(windowSize[1] * 0.8))) 
            imgbytes=cv2.imencode('.png', frame)[1].tobytes() #ditto
            window['frameViewer'].update(data=imgbytes, visible=True)
            window['_CANVASCONTAINER_'].update(visible=True)
        elif step == 2:
            window['_ACTIVITYZONE_'].update("3) Choose cutted SER File destination")
            window['frameViewer'].update(data=imgbytes, visible=False)
            window['_CANVASCONTAINER_'].update(visible=False)
            window['_SAVINGASCONTAINER_'].update(visible=True)
            window['_NEXT_'].update('Export')
        elif step == 3:
            window['_ACTIVITYZONE_'].update("Rendering...")
            outputFile = values['SavingLocation']
            destFile  = Serfile(outputFile, NEW =True)
            destFile.createNewHeader(serfile.getHeader())
            beginAt = int(firstFrame)
            endAt = int(lastFrame)
            k=1
            window['_SAVINGASCONTAINER_'].update(visible=False)
            window['progressbar'].update(current_count=0, visible=True)
            serfile.setCurrentPosition(beginAt)
            for i in range(beginAt,endAt):
                window['progressbar'].UpdateBar(i * 1000 / serfile._length)
                frame=serfile.read()[0]
                destFile.addFrame(frame)

                k+=1

            break
            
    
    
    elif event == "_PLAYPAUSE_":
        playing = not playing
        if playing:
            window['_PLAYPAUSE_'].update('', button_color=sg.TRANSPARENT_BUTTON,image_filename=image_pause, image_size=(50, 50), image_subsample=2)
        else:
            window['_PLAYPAUSE_'].update('', button_color=sg.TRANSPARENT_BUTTON,image_filename=image_play, image_size=(50, 50), image_subsample=2)
    
    elif event == "_FASTER_":
        if framesToAdd == -1 and timeDelay >= 1:
            framesToAdd = 1
        if framesToAdd < 0:
            if framesToAdd == -1:
                timeDelay *= 2
            else:
                framesToAdd //= 2
        else:
            if timeDelay > 0.02:
                timeDelay /= 2
            else:
                framesToAdd *= 2
    
    elif event == "_SLOWER_":
        if framesToAdd == 1 and timeDelay >= 1:
            framesToAdd = -1
        if framesToAdd > 0:
            if framesToAdd == 1:
                timeDelay *= 2
            else:
                framesToAdd //= 2
        else:
            if timeDelay > 0.02:
                timeDelay /= 2
            else:
                framesToAdd *= 2
        
    elif event == "_BBRACKET_":
        if actualFrame < lastFrame:
            firstFrame = actualFrame;
            moveBracketTo(bracketBeginning, firstFrame, serfile._length)
    
    elif event == "_EBRACKET_":
        if actualFrame > firstFrame:
            lastFrame = actualFrame;
            moveBracketTo(bracketEnd, lastFrame, serfile._length)
        
    elif playing:
        if time.time() - lastTime > timeDelay:
            lastTime = time.time()
            numFramePrinted = actualFrame + framesToAdd
            
            if 0 <= numFramePrinted < serfile._length:
                moveTimeMarker(timeMarker,numFramePrinted-framesToAdd,numFramePrinted,serfile._length)
                actualFrame = numFramePrinted
                frame, ret = read_frame(numFramePrinted)
                frame = cv2.resize(frame, (int(windowSize[1] * ratio * 0.8), int(windowSize[1] * 0.8))) 
                imgbytes=cv2.imencode('.png', frame)[1].tobytes() #ditto
                window['frameViewer'].update(data=imgbytes)
                window['_NUMFRAME_'].update("%i"%(ret))
            else:
                actualFrame = 0
                moveTimeMarker(timeMarker,numFramePrinted-framesToAdd,0,serfile._length)
                window['_NUMFRAME_'].update("0")
                playing=False
                window['_PLAYPAUSE_'].update('', button_color=sg.TRANSPARENT_BUTTON,image_filename=image_play, image_size=(50, 50), image_subsample=2)
                numFramePrinted = 0
    #if event!="__TIMEOUT__" : 
        #if "canvas" in event:
            #print(event)
    
    window.refresh()

window.close()
