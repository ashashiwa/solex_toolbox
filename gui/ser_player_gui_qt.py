#!/usr/bin/env python
import sys
import PySimpleGUI as sg

import cv2
import numpy as np
from sys import exit as exit

sys.path.insert(0,'..')
from serfilesreader import Serfile
import time
#print("usage : setfilesreader.py -f file.ser")


"""
Demo program that displays a webcam using OpenCV
"""
def main():
    sg.ChangeLookAndFeel('LightGreen')
    # define the window layout
    layout = [[sg.Text('Play ser Demo', size=(40, 1), justification='center', font='Helvetica 20')],
              [sg.Text('Nom du fichier', size=(25, 1)), sg.InputText('',size=(20,1),key='-FILE-'), sg.FileBrowse('Open',file_types=(("SER Files", "*.ser"),),initial_folder='.')]  ,
              [sg.Image(filename='', key='image', size=(200,700)),sg.Text('Frame number : ', size=(20,1), key='-NUM-')],
              [sg.Button('Play', size=(10, 1), font='Helvetica 14'),
               sg.Button('Stop', size=(10, 1), font='Any 14'),
              sg.Button('Exit', size=(10, 1), font='Helvetica 14'),
               sg.Button('About', size=(10,1), font='Any 14')]]
    #if filename==None else [sg.Text('Nom du fichier', size=(25, 1)), sg.InputText(default_text=filename if filename!= None else "" ,size=(20,1),key='-FILE-')]
    # create the window and show it without the plot
    window = sg.Window('Demo Application - OpenCV Integration - Ser Reader',
                       location=(800,400))
    window.Layout(layout)

    # ---===--- Event LOOP Read and display frames, operate the GUI --- #
    #cap = cv2.VideoCapture(0)
    playing = False
    
    
    cptr_fps=1
    t_begin = time.time()
    while True:
        
        event, values = window.Read(timeout=0, timeout_key='timeout')
        if event == 'Exit' or event is None:
            sys.exit(0)
            pass
        elif event == 'Play':
            playing = True
            filename=values['-FILE-']
            try : 
                serfile = Serfile(filename)
            except : 
                playing=False
                
        elif event == 'Stop':
            playing = False
            
        elif event == 'About':
            sg.PopupNoWait('Made with PySimpleGUI',
                        'www.PySimpleGUI.org',
                        'Little ser player based on serfilesreader library',
                        keep_on_top=True)
        if playing:
            #reading frame by frame
            frame, ret = serfile.read()
            if ret <0 : #end of video
                playing=False
            #resize
            cptr_fps+=1
            if time.time()-t_begin>1.0 : 
                fps = cptr_fps
                cptr_fps=1
                t_begin = time.time()
            imgbytes=cv2.imencode('.png', frame)[1].tobytes() #ditto
            window.FindElement('image').Update(data=imgbytes)
            window.FindElement('-NUM-').Update("Frame number %i, %i fps"%(ret, fps))


    
main()
exit()
