# ser_toolbox

lot of little softwares helping in managing SER files from Sol'Ex spectrograph
[http://astrosurf.com/solex-> http://astrosurf.com/solex]

**ADVERTISSMENT :**
this is alpha pre-code. It's only used in educational purpose. 
If you want to treat your Sol'Ex files and do some SCIENCE, please **DO NOT USE** this toolbox.
Some other software are : better documented, easier to use, more accurate.
If you are not python or not confortable with scipts language, please use ISIS, from Christian BUIL.
[treatment-> http://www.astrosurf.com/solex/sol-ex-traitement.html]

## we use pyserfilesreader : 
https://gitlab.com/ashashiwa/pyserfilesreader

pip install serfilesreader

## You will find here : 
- a cutter (something to cut some part of SER files to make a new one) : 
__cd gui__
__python ser_cutter_gui_qt.py__

- some piece of code to manage SER files issues froms Spectrohéliograph
__python toolbox_solex.py__  PLEASE READ THE DOC BELOW

## Dependancies
In two lines : 
### Update pip && install Dependancies
__python -m pip install -U pip__

__python -m pip install PySimpleGUI opencv-python scikit-image numpy matplotlib scipy functools__

### PySimpleGUI:
__python -m pip install -U PySimpleGUI__

### opencv :
__python -m pip install -U opencv-python__

### scikit-image : 
__python -m pip install -U scikit-image__

### numpy : 
__python -m pip install -U numpy__

### matplotlib
__python -m pip install -U matplotlib__

### scientific-python : 
__python -m pip install -U scipy__

### functools
__python -m pip install -U functools__

# Documentation
toolbox_solex provides some tools to make your own tests
## script example : 
        
    rep = "/home/user/Téléchargements"                                                      #répertoire où sont les fichiers "ser".  A CHANGER
    fichier =  'Sun_104633.ser'                                                             ##A CHANGER. 
    image_centre_raie= combine_all_frames_from_file_auto(os.path.join(rep,fichier), dx=0)   ###no decalage
    image_centre_raie_t = remove_transversalium(image_centre_raie)                          #enleve le transversalium
    image_centre_raie_tn = normalized(image_centre_raie_t)                                  #normalise pour affichage
    image_centre_raie_tnc = circularize(image_centre_raie_tn)                               #circularise l'image
    show(colorize(img16b_to_8b(image_centre_raie_tnc)))                                     #colorise en orange
    nom_png = "Sun_104633.png"
    save_to_png(colorize(img16b_to_8b(image_centre_raie_tnc)), nom_png)                     sauvegarde en png.
