#!/usr/bin/env python
import PySimpleGUI as sg
from random import randint
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, FigureCanvasAgg
from matplotlib.figure import Figure
from toolbox_solex import *

# Yet another usage of MatPlotLib with animations.

def draw_figure(canvas, figure, loc=(0, 0)):
    figure_canvas_agg = FigureCanvasTkAgg(figure, canvas)
    figure_canvas_agg.draw()
    figure_canvas_agg.get_tk_widget().pack(side='top', fill='both', expand=1)
    return figure_canvas_agg

def main(filename):
    monfichier = Serfile(filename)
    #image_norm = sum_frames(monfichier,800,3200,True,0)
    #frame = returnFrameROI(gaussian_v(image_norm,12),(0,101,87,800))
    #print(frame.shape[0])
    monfichier.setCurrentPosition(monfichier.getLength()//2)
    frame = monfichier.read()[0]

    # define the form layout
    layout = [[sg.Text('profil par ligne', size=(40, 1),
                justification='center', font='Helvetica 20')],
              [sg.Canvas(size=(640, 480), key='-CANVAS-')],
              [sg.Slider(range=(0,frame.shape[0]-1), size=(60, 10),default_value=0,
                orientation='h', key='-SLIDER-')],
              [sg.Button('Exit', size=(10, 1), pad=((280, 0), 3), font='Helvetica 14')]]

    # create the form and show it without the plot
    window = sg.Window('Read line by line of a frame',
                layout, finalize=True)

    canvas_elem = window['-CANVAS-']
    slider_elem = window['-SLIDER-']
    canvas = canvas_elem.TKCanvas

    # draw the initial plot in the window
    fig = Figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel("X axis")
    ax.set_ylabel("Y axis")
    ax.grid()
    fig_agg = draw_figure(canvas, fig)
    # make a bunch of random data points
    
    for i in [430,844,7]: 
        frame1 = frame[i]
        minimum = np.min(frame1)
        print(minimum)
        valeur1 = int(np.where(frame1 == minimum)[0])
        
        
        np.save("raie_vers_%s_valeur_%s.npy"%(str(minimum), str(valeur1)), frame1)
    
    while True:

        event, values = window.read(timeout=10)
        if event in ('Exit', None):
            exit(69)

        ax.cla()                    # clear the subplot
        ax.grid()                   # draw the grid
        data_points = frame[int(values['-SLIDER-'])] # draw this many data points (on next line)
        ax.plot(range(len(data_points)), data_points,  color='purple')
        
        fig_agg.draw()

    window.close()

if __name__ == '__main__':
    main('/home/jb/Téléchargements/11_40_09.ser')
